---
label: Dynastra Tabletop
icon: info
order: 99
---
## Notice: We're changing name!
!!!warning Yes, we're changing
Dynastra Tabletop is being renamed to **AscensionVTT** after its Pre-Alpha release.
!!!
[![Netlify Status](https://api.netlify.com/api/v1/badges/596d2ea2-ac5e-4e76-a5d7-2ecf4c452964/deploy-status)](https://app.netlify.com/sites/dynastra-tabletop-docs/deploys)