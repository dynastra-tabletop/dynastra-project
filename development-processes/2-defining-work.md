---
label: Step 1 - Defining work
---
This document will work through a basic process of defining the work you are doing. If you are going to make a feature, it's worth sticking around and becoming a Dynastra Core Team member, but if you want to develop a feature as a lone wolf, we'll still support you.

Just make sure it's a feature you think we can merge soon, rather than focus on some feature that isn't as needed just yet.

# Steps
1. Define what I'm adding
2. Define which parts of the stack need to be updated to accomplish it
3. Tell us when you think it should be implemented

## 1. Define what I'm adding
!!!success A good example
When the dice roll display is shown, I want to add in a "header" so that weapons, spells, and items are displayed.
!!!

## 2. Define which parts of the stack need to be updated.
Don't worry if you're not sure, just put as much info as you can and we can help figure out what else needs updating.

!!!success A good example
1. The server needs an extra property of "displayType"
2. The client needs a header on the existing dice roll display
3. The client needs a dialog window asking which kind of roll it is
!!!

## 3. Tell us when you think it needs to be implemented
For instance, look at our development roadmap. If dice rolls aren't added in yet, then it'd be *after* dice rolls. If you look at the features planned and think "hey, this is way more important!", that's fine, you might be right! Just pick a point when you think it's needed, and then show us in the Slack and we'll quickly work around your request!