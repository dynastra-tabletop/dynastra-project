---
label: Step 2 - Prototyping
---

## Process
1. Write a basic prototype before coding
2. Discuss with other developers

### 1. Write a basic prototype before coding

When prototyping, it's better not to commit to actually writing code or HTML unless you can help it. If you can use a prototyping tool or even just draw things in your favourite piece of software, even better. The idea is at this point just to work through how the feature would work, and outlining the process. This means that then if there are any steps that need to be expanded or trimmed down, then we can figure that out before too much work is done.

### 2. Discuss with other developers
Post in the slack chat and spam people, ask them to take a look and see how they think it would work.  Take the feedback into consideration.