---
label: Process overview
---

## Development processes
```mermaid
graph LR 
    A[1. Define] --> B[2. Prototype] --> C[3. Refine] --> D[4. Develop] --> E[5. Test] --> F[6. Deploy] 
```

## 1. Define - Define what you're doing
Before starting work, just take a look at our **requirements** and **planned releases** so you can get an idea of where your feature or addition might be added. 

For instance, you might want to work on a "character sheet" feature, and that's awesome, but before we do that, we might need to implement dice rolls first! And after that, we might then have other work we need to do first. The idea is we want to come to the conclusions together about what work goes first, second, third, and so on. 

The priority of which feature comes first should be guided by the community but grounded by technical concerns. Don't rush to add a feature if it means it'll need to be revised a lot later.

## 2. Prototype - show off what you want to add
Now's the time to get creative and make a basic prototype. I wouldn't necessarily start with code - this could even just be drawings on paper, boxes drawn in an editor of some kind with a description. Once you've done that, jump into the Slack and get a quick discussion between other developers to ensure that things are consistent. Check this site for any specifications but, if there aren't any, use your best judgment and ensure visual elements are kept similar and consistent. 

## 3. Refine - get other devs to help
Once you've spoken to other devs, they might have some ideas on the flow or display of your prototype. That's cool - if they do, incorporate their feedback into your work then send them updated screenshots and descriptions. Don't rush into coding just yet unless it's necessary to visualise how a flow works in the UI.

## 4. Develop
Do what you do best. Before you do this, create a copy of your final design and write up some basic epics and user stories to describe it, and then start work on that. Make sure you split up your feature into multiple parts that are each functional, and which 

## 5. Test
So, we will almost certainly have no testers, so this is a loose science. The way we can get around needing testers is by:
1. Writing tests to cover *all* the code you wrote, or at least most of it. 
2. Creating some basic end-to-end tests (such as UI tests) to cover off the functionality

## 6. Deploy
This shit is automatically done once merged.