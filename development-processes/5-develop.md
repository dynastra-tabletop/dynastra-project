---
label: Step 4 - Develop
icon: repo
---

## Process
1. Write epics and user stories and split tasks (optional)
2. Develop and write unit tests together

## 1. Write epics and user stories
Okay, I know that development is the cool thing you want to do, but we need to be clear on what we're doing, and epics and user stories help clarify this. 

* **Epics** - Provide a high level "goal" for the software (e.g. "Allow the user to log in")
* **User stories** - The constituent parts used to implement an epic

### Epics
#### Existing epics
Most features will already be covered broadly by epics, so you will simply need to find the one which covers work you're doing. So, for example:

* **Feature**-  "Add a title to a dice roll" 
* **Epic** - Rolling of dice
* **User story** - As a Player User I must be able to see the type of dice roll, so I can see what the dice was rolled for

#### Features with no existing epics
Let's say you're working on a feature which is completely new. You will need to write an epic covering the entire feature you're working on. However, most of the time you'll have an epic covering what you're doing.
* **Feature name** - Riffwobble integration
* **Epic** - Plunder Riffwobbles
* **User story** - As a Player User, I must be able to plunder Riffwobbles, so that I can live forever.

### User stories
#### Naming User Stories
I know they sound stupid, but writing User Stories from a perspective of a user allows us to see what work is needed for what thing. So for example, if we want to add in a feature to search character sheets

* **Feature**: View a character sheet
* **Epic**: Character sheets
* ***User Story** As a Dungeon Master User, I must be able to search character sheets, so that I can manage a Role Playing Game

#### User stories
User stories should generally when "added up" (e.g. If you look over all of them), equate to the epic, which is generally a feature. 

## 2. Develop and write unit tests together
Once you've got your user stories, write unit tests as you code to cover those, to make sure your code matches what you planned to do. If it doesn't, change the stories and epics if you realise it can't be done, or needs to be changed.


