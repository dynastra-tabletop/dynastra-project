---
label: Step 3 - Refine your prototype
icon: repo
---

This process is basic - take the feedback that you've received in Slack and incorporate it into your prototype. Change until reservations are cleared - if someone has lots of reservations, work through them in the chat. If there is nobody available to do this, then use your best judgment and ensure that you're following the processes here. 