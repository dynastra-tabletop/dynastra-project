## Front-end
* **Platform** - Angular (Latest)
* **Front-end framework** - Angular Material
* **Unit/Integration test platform** - Karma/Jasmine
* **Component test platform** - Storybook
* **End-to-end test platform** - CypressJS
* **Sockets** - SignalR
* **Canvas library** - KonvaJS

## Back-end
* **Platform** - Microsoft .NET 5
* **Contracts** - OpenAPI
* **Sockets** - SignalR
* **Storage** - RethinkDB
* **Unit/Integration test platform** - XUnit & Moq
