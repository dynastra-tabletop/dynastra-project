---
label: User stories
---
## Roles
* Visitor - A user visiting the system with no role assigned (not authenticated)
* Authenticated User - A user browsing but neither a Player User nor a Dungeon Master User
* Dungeon Master User - A user running a role playing game for others to join
* Player User - A user playing a role-playing game started by a Dungeon Master User
* System Administrator - The person running the Dynastra Tabletop instance

## DN-EPIC-0 - General
* As a project, I must have tools for performing Behaviour Driven Development, so that tests written for the project match clearly defined goals

## DN-EPIC-1 - Logging in to the System
* DN-E1-US001 - As a Visitor, I must be redirected to a login page, so that I can authenticate with the system
* DN-E1-US002 - As a Visitor without proper authentication, I must not be able to view any part of the system, so that I abide by the content policy of the System Administrator
* DN-E1-US003 - As a Visitor with credentials which are locked/expired/disabled/banned, I must not be able to view any part of the system, so that I abide by the content policy of the System Administrator
* DN-E1-US004 - As an Authenticated User, if I am using the system by moving my mouse or clicking elements of the UI, my login token must be refreshed, so that I can continue using the UI without interruption
* DN-E1-US005 - As an Authenticated User, if I am not using the system by moving my mouse or clicking elements, or the tab is inactive, my login token must not be refreshed, and my token must expire, so that my session cannot be hijacked by a proximate user
* DN-E1-US006 - As an Authenticated User, if my token expires, I must forced to log into the system again, so that my session cannot be hijacked
* DN-E1-US007 - As an Authenticated User, I must be able to log out of the system and my login token be forcibly expired, so that my session can be concluded and not further used.
* DN-E1-US008 - As a Visitor, I must be redirected to a generic error page if the authentication provider is unavailable or an unexpected error occurs, so that I have an indication of when or whether I can log in.
* DN-E1-US009 - As an Authenticated User, Dungeon Master User, or Player User, having logged into the system, I must be redirected to a post-login page, so that I can be welcomed to the system
* DN-E1-US010 - As a Visitor, having logged out, I must be redirected to a logout page, so that I can visually confirm as being logged out from the system
* DN-E1-US011 - As a Visitor, having just logged out, I must be able to log into the interface by means of a "quick login" display, so that I can re-log-in to the system
## DN-EPIC-2 - Management of the User Profile
* DN-E2-US001 - As an Authenticated User, Dungeon Master User, or Player User, I must be able to view my profile based on information from the OIDC server, so that I can view my current logged in status.
* DN-E2-US002 - As an Authenticated User, Dungeon Master User, or Player User, I must be able to click a link on my profile display and view the "edit profile" page provided bvy the external OIDC provider
* DN-E2-US003 - As an Authenticated User, Dungeon Master User, or Player User, I must be able to view a profile image for my current logged in user, so that I can express my individuality


## DN-EPIC-3 - Management of System Roles
* DN-E3-US001 - As an Authenticated User, I must be allocated a role of Dungeon Master User when I create a Role Playing Game, so that I can manage the Role Playing Game as a Dungeon Master User
* DN-E3-US002 - As an Authenticated User, I must be allocated a role of Player User when I join a Role Playing Game, so that I can play the Role-Playing Game as a player
* DN-E3-US003 - As a Dungeon Master User, I must have certain DM-Only elements visible within a Role Playing Game only to me, so that I can maintain secrecy
* DN-E3-US004 - As a Player User, I must not be able to see DM-Only elements, so that I cannot change the Role Playing Game configuration
* DN-E3-US005 - As a Dungeon Master User, I must be able to add other users to the Dungeon Master User Role, so that that user can become a Dungeon Master and run the game if I am absent, or to share responsibilitiess

## DN-EPIC-4 - Listing, Searching and Displaying of Role-Playing Games
* DN-E4-US001 - As a Dungeon Master User, I must be able to create a "Sharing Link" for a Role Playing Game I have created, so that other people can join my game
* DN-E4-US002 - As a Player User, I must be able to open a "Sharing Link" to enter a game provided to me, so I can play the Role Playing Game
* DN-E4-US003 - As a Dungeon Master User, I must be able to toggle making my game invite-only, so I can restrict who can play my Role Playing Game
* DN-E4-US004 - As a Dungeon Master User, I must be able to toggle making my game visible to the public, so I can restrict who can see and play my Role Playing Game
* DN-E4-US005 - As a Player User, Dungeon Master or Authenticated User, I must be able to view a list of visible Role Playing Games, so that I can find a Role Playing Game I wish to play.

## DN-EPIC-5 - Creation and Management of Role-Playing Games
* DN-E5-US001 - As an Authenticated User, I must be able to create a Role Playing Game, so that I can become a Dungeon Master User
* DN-E5-US002 - As a Dungeon Master, I must be able to edit the details of a Role Playing Game, so that Player Users, Dungeon Master Users, and Authenticated Users can distinguish it from other Role Playing Games
* DN-E5-US004 - As a Dungeon Master, I must be able to delete a Role Playing Game, so that I can end a campaign or remove one as I see fit

## DN-EPIC-6 - Running Role Playing Games as a Dungeon Master User 
* DN-E6-US001 - As a Dungeon Master User, I must be able to place Tokens on the Game Board, so that Dungeon Master Users and Player Users can move them around the Game Board
* DN-E6-US002 - As a Dungeon Master User, I must be able to delete Tokens on the Game Board, so that they disappear
* DN-E6-US003 - As a Dungeon Master User, I should be able to obscure part of the Game Board from Player Users so that tokens underneath it are not visible, so that users cannot see surprises I lay in wait for them
* DN-E6-US004 - As a Dungeon Master User, I must be able to move individual players between maps, so that they can progress through the Role Playing Game
* DN-E6-US005 - As a Dungeon Master User, I must be able to move all players between maps, so that I can move users more quickly

## DN-EPIC-7 - Playing Role Playing Games as a Player User 
* DN-E7-US001 - As a Player User, I must not be able to see areas obscured from me by the Dungeon Master, so that surprise is maintained
* DN-E7-US002 - As a Player User, when I am moved between maps, I should be informed I am being moved, so that I can see where I am moving to
* DN-E7-US003 - As a Player User, I should be able to see which map I am currently in, so that I can understand the place I am at in the Role Playing Game.
* DN-E7-US004 - As a Player User, I should be able to play on a map separately from other players, so that side-stories can be played
* DN-E7-US005 - As a Player User, I should be able to see other connected players in a role playing game and be able to view their details.

## DN-EPIC-8 - Management of Locations (Maps) as a Dungeon Master User
* As a Dungeon Master User, I must be able to add "Maps" to a Role Playing Game which outlines an area within the game, so that players can move around
* As a Dungeon Master User, I must be able to delete "Maps" from a Role Playing Game, so that I can remove them at my will
* As a Dungeon Master User, I must be able to perform changes to objects on the Map and have them only updated when I have finished editing, so that users do not see many changes to the layout of the map

## DN-EPIC-9 - Navigation of Locations (Maps) as a Player User and Dungeon Master User 
* As a Player User or Dungeon Master User, I must be able to move Tokens around the Game Board, so that I can approximate the Token moving

## DN-EPIC-10 - Management of Characters as a Dungeon Master User 
* As a Dungeon master User, I must be able to edit details a about a given Character's token, so that the information can be updated

## DN-EPIC-11 - Management of Characters as a Player User 
* DN-E11-US001 -  As a Player User, I must be able to manage Characters which I can use in Role Playing Games, so that I can progress the game.
* DN-E11-US002 - As a Player User, I must be able to view tokens which are assigned to me and their basic information, so that I can see which character I am playing
* DN-E11-US003 - As a Player User, I must be able to change the details of my own token, so that I can update its information as I see fit

## DN-EPIC-12 - Communication between Player Users and Dungeon Master Users
* As a Player User or Dungeon Master User, I must be able to communicate via text chat to other users, so that I can convey information to others
* As a Player, I must be sure that my data is not stored, so that my personal data is not harvested by government agencies

## DN-EPIC-13 - Dice Rolling
* DN-E13-US001 - As a Dungeon Master User, I must be able to roll dice as a token, so that the token appears to have rolled a dice
* DN-E13-US002 - As a Player User or Dungeon Master User, I must be able to roll dice by entering commands in the chat display, so that I can roll dice quicker
* DN-E13-US003 - As a Player User or Dungeon Master User, I must be presented with a list of common dice rolls, so that I can roll dice quicker
* DN-E13-US004 - As a Player User or Dungeon Master User, I must be able to see a list of dice rolls that have been performed in a game so that I can track game actions

## DN-EPIC-14 - The Game Board
* DN-E14-US001 - As a Player User, I must be able to lock the background layer so that map images do not get dragged as I drag tokens.
* DN-E14-US002 - As a Player User, I must be able to to measure distances on the game grid and determine the size of each grid square so I can accurately determine attacks and other distance-related actions in role playing games.