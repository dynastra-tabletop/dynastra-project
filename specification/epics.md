| Epic name | Title | 
|---|---|
| DN-EPIC-1  | Logging in to the System  |
| DN-EPIC-2  | Management of the User Profile  |
| DN-EPIC-3  | Management of System Roles  |
| DN-EPIC-4  | Listing, Searching and Displaying of Role-Playing Games  |
| DN-EPIC-5  | Creation and Management of Role-Playing Games |
| DN-EPIC-6  | Running Role Playing Games as a Dungeon Master User  |
| DN-EPIC-7  | Playing Role Playing Games as a Player User  |
| DN-EPIC-8  | Management of Locations (Maps) as a Dungeon Master User |
| DN-EPIC-9  | Navigation of Locations (Maps) as a Player User and Dungeon Master User  |
| DN-EPIC-10  | Management of Characters as a Dungeon Master User  |
| DN-EPIC-11  | Management of Characters as a Player User   |
| DN-EPIC-12  | Communication between Player Users and Dungeon Master Users   |
| DN-EPIC-13 | Dice Rolling |
