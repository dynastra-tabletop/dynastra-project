---
label: Dynastra Project Processes
icon: rocket
---

```mermaid
graph LR 
    A[1. Define next release version] --> B[2. Discuss features] --> C[3. Scope features] --> D[4. Name release] --> E[5. Work on release] --> F[6. Release]
```

## 1. Define next release version
The Dynastra Core Team define the next release version (e.g. 0.0.2) which will be scoped. 

## 2. Discuss features
1. The Dynastra Core Team discuss which features are required in the next release and outline them with the community, explaining why other features might not be possible due to technical constraints. 
2. The features to be included are documented in this site, giving proper attribution to community members who suggested them.


## 3. Scope features
1. If features are too big then they are scoped to the next release. 
2. Dynastra Core Team has the right to move features out they feel are untenable. For features moved to the next release, the Dynastra Core Team have to provide reasons why and document them on this site. 

## 4. Name release
1. Each version must have a name.
2. The community (or failing that the sole developer) decide or vote on a name for the release. 
3. The release is then named and included in documentation.

## 5. Work on release
The release is worked. 

## 6. Release
1. Once all work is completed on the release then the first release candidate is released to the community (e.g. x.x.x-rc1)
2. The community tests the features in this release and, if features added don't conform to the features agreed, the "rcxx" increases (e.g. x.x.x-rc2, rc3, etc)
3. Once the community and developers agree the release is final, then the release goes to being (e.g. x.x.x) and is tagged for general release