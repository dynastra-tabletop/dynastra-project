---
label: Dynastra Tabletop Team
icon: person
order: 99
---

Dynastra Tabletop is Free and Open Source Software project which is run entirely by volunteers. The continued development is supported by staffed members of the project, but like all FOSS projects, anyone can contribute whatever they like. We prefer if it helps our roadmap, but you can do what blows your hair back, babe.

## Core team

| Jordán Craw (@jordancraw)  | Marco Dizo (@mdizo) |
|---|---|
| ![Profile](https://gitlab.com/uploads/-/system/user/avatar/9872049/avatar.png?width=400)| ![Profile](https://gitlab.com/uploads/-/system/user/avatar/5870979/avatar.png?width=400) |
| **Role**: Founder (Coder, Architect, all that shit) | **Role**: UX/UI Lead |
| **Responsibilities**: <ul><li>Overall project control (Epics, user stories, documentation)</li><li>Primary Client-side developer</li><li>Primary Server-side developer</li></ul> | **Responsibilities**: <ul><li>Overall control of UX/UI design</li><li>Branding and logos</li><li>Overall control of UI/UX-facing user stories and epics</li><li>Website</li><li>Documentation design control</li></ul>|

## Game support

Dynastra is also supported by communities and individual who give technical direction for certain rulesets:

| [RED Winter](https://redwinter.net/) |
|---|
| ![Image](./red-winter.jpg) |
| The RED Winter TTRPG Discord Community has been invaluable in providing feedback, direction and support to us around Cyberpunk RED and Cyberpunk in general! Thank you! <3 |