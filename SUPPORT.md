## Open Source Support
Dynastra Tabletop would like to express thanks to the following organisations for their support:

### BrowserStack
![BrowserStack](./support-browserstack.png)

*BrowserStack provides Dynastra Tabletop with free Open Source access to the BrowserStack platform for cross-platform testing. Thanks so much for your support!*

### Netlify
![Netlify](./support-netlify.png)

*Netlify provides Dynastra Tabletop with the Netlify Pro (Open Source) plan, which is used for hosting the documentation and will eventually be used to host the website after the alpha release. Thank you so much for your support!*

### JetBrains
![JetBrains](./support-jetbrains.png)
*JetBrains provides Dynastra Tabletop developers with full use of the JetBrains products for use within the project, including IntelliJ IDEA, Rider, ReSharper, and the rest of the awesome JetBrains product family. Thank you so much for your support!*