---
label: Project charter
icon: file-badge
order: 98
---

Our goal at Dynastra Tabletop is to provide a **Free Software Virtual Tabletop** experience for everyone to use and distribute freely. We work together to accomplish the goal of providing a Virtual Tabletop system which is:

1. Of good quality; reliable
2. Good-looking
2. Easy to use
3. Scalable and interoperable
4. Accessible

In order to accomplish these goals, and to deliver software, we establish a project which:

1. Allows people to contribute openly to the project in any capacity they wish
2. Details its direction and processes clearly for contributors
3. Is driven democratically by its community - users and contributors
4. Provides a clear direction and processes
5. Operates in the open
6. Values collaboration and celebrates difference

In order to ensure that we have a healthy community, we ensure that:
1. No processes or discussions had by the development team are held in private - we are open, and act in the open.
2. The development team and the users of the software are one entity - our community.