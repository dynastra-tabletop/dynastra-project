---
label: About The Project
icon: info
---

# Communications
* Slack - http://dynastratabletop.slack.com


# Why no logo, why no domain name yet?
!!! :zap: Let's try "Gardening" :zap:
You can look at The World of Ice and Fire…my book of fake history…and there’s a lot of stuff in there about what led up to Game of Thrones, all the preceding kings and the conflicts of their era and all that, and people have said to me, “You have 50 other novels in here. You could write a novel about Aegon’s conquest, and all that.” And yes, I could, but I don’t think I will because I already wrote those 20 pages about Aegon’s conquest and that’s the important stuff that happened in Aegon’s conquest…I’ve made up the fun stuff, and the twists and the characters and the cool lines of dialogue, you know? I skip over the boring lines of dialogue…The few times I actually quote dialogue it’s great lines of dialogue said by famous historic figures at fraught points. There’s very little of “Hey, what’s for dinner?”

And of course I love the fully fledged novels, so that’s why I’m mostly a gardener when I write a novel. I know that highlights. I haven’t written them yet, though…and I’m getting from one cool thing to another cool thing.
!!!

The idea of "gardening" is appealing because sometimes when you've written out all the project, given it a logo, given it a website, and talked a lot of shit, it feels like the idea is already chewed meat. I'm going to wait for the alpha to be finished, mainly because I want to make sure this software is up and running so that I have something that I can run Cyberpunk on, and because it'd be very nice to show Jimmy Dipshit how trivial his efforts have been.
