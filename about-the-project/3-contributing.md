---
label: Contributing
icon: people
---

Dynastra Tabletop is a Free Software project, and developing on the project is as simple as taking a piece of work and working on it. There aren't any long, complex "let's have a chat" sessions, calls, or other things, you can just pick up work and work on it.

That said, we also want to make people who choose to develop here at Dynastra Tabletop feel welcome and have their work celebrated. This means that if you want to stick around and move the project forward as fast or as slow as you want, we'll try to accomodate the level of effort you're putting in. 

## Types of contributor
In this guide, we'll refer to two types of contributor:
1. Ad-hoc contributor
2. Dynastra core team

### Ad-hoc contributor
An ad-hoc contributor is someone who just comes along and provides things to the project. They can contribute on whatever they want, provided it's not already got someone working on it. If you're just wanting to fix a bug, add some value, then go for it.

### Dynastra core team
The core team is intended to be for people who want to contribute more solidly to the project, form a part of the community, and contribute code long-term and help work on specific features, and so on. This means that for this reason there is a little bit more of a process so we can celebrate you, and so that the community can get to know you!

## Processes
The processes we have are documented clearly here so that, if you want to start work, you're not limited and waiting for someone else to give you instruction. We try to make sure that all of this documentation is clear and straightforward, but if there is something lacking or unclear, it is never a problem to change it for the better.

## Attribution
If you contribute to this project, you have the right to add in your own message and details, which will be displayed on the Dynastra Tabletop website. If you are a Dynastra Core Team, then you can treat yourself to a nice full-page on the site so members know who you are and what you're about.

## Making contributions to software (programming tasks)
Making a contribution doesn't need there to be any kind of formal process followed. However, it's better to try and contribute to what is important to the project at the time so you have an idea of when your work will be added to the project. 

### General bugfixing
If you've found a bug and it's relatively minor, and want to fix it, don't be shy, just create a bug in the repository and then go right ahead and fix it if you're able. If you're making changes to code, you won't get your Merge (Pull) Request merged unless you meet the **Pull Request Criteria** but don't worry, the process will be automated to help you out! 

