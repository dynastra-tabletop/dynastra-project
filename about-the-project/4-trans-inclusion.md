---
label: Trans inclusion policy
icon: heart-fill
---

As a first thing, Dynastra Tabletop states clearly that Trans people are human beings deserving of respect, love, and equality. Dynastra Tabletop will do all it can to ensure that trans rights are maintained within this project, and will not take any transphobic comments lightly, regardless of contributor status or tenure. 

Dynastra Tabletop has a zero-tolerance approach towards discrimination and harassment based on gender identity, gender expression or gender history.

## Zero-tolerance to anti-trans attitudes
Dynastra Tabletop has a zero-tolerance approach towards discrimination and harassment based on gender identity, gender expression or gender history. Inappropriate behaviour or language may constitute discrimination, harassment, bullying or victimisation. Discrimination including harassment, third party harassment (encouraging others to harass members of this community) and victimisation are absolutely unacceptable. Contributors and project leadership are responsible for taking timely action where misconduct occurs on the grounds of an contributor's gender identity, in line with the harassment policy. This will be monitored and followed up.

## Definitions
"Trans" or "transgender" describes people whose gender identity differs from their sex assigned at birth. They are umbrella terms covering people who:
* are intending to undergo, are undergoing, or have undergone gender reassignment at any stage;
* identify as having a gender different from that which they were assigned at birth and are planning or have had medical interventions such as hormones or surgery;
* identify as having a gender different from that which they were assigned at birth, but who are not planning any medical intervention; and/or, 
* are non-binary – that is, they are not solely male or female.  They may define themselves as both, neither or something entirely different.  They may or not have medical interventions to align their body with their non-binary gender identity.

These are not mutually exclusive alternatives.

## Transitioning 
"Transitioning" is the process undertaken by a trans person in order to bring their gender presentation into alignment with their gender identity. This often involves dressing differently, using a different name and pronoun (eg she, he or they) and changing official documentation. It may involve various types of medical or surgical treatment, although this is not the case for all trans people. 

Dynastra Tabletop recognises there is no right or wrong way to transition and is committed to supporting each individual in their decisions.
Dynastra Tabletop commits to reassuring all staff that they will be supported and respected. The transition process will be led by the individual concerned.

## Reporting
If you are a subject of any form of discrimination in this regard, contact the leadership team immediately. The code of conduct will be followed in instances of transphobia and anti-trans behaviour.

## Further reading and suggestions
1. https://www.tuc.org.uk/resource/how-be-good-trans-ally-work
2. https://www.tuc.org.uk/resource/transphobic-hate-incidents-and-crimes
