---
label: Adopting new processes
icon: repo
---
!!!info A personal note..
I don't want to go into too much detail about my personal life, but, let's just say I have an outlook in which, I do not value hierarchy much. For that reason we have to maintain a position between two points when it comes to doing, and documenting processes:

1. I don't know what the fuck I'm doing 

and

2. I need to follow a process for farting in my chair

Let's strive to keep a balance between the two (e.g. "Hey, let's only do this if necessary, but also, let's be clear so others can follow our practices") :D
!!!
When we are thinking about processes, we really want to make sure we're doing things so that they:
1. Add value 
2. Improve the quality of the finished product
2. Improve efficiency and throughput of work
3. Prevent regressions and inconsistencies in design.

If you want to add a process, let's discuss whether we absolutely need it, but at the same time, if there are things going on which we don't have any documented process for, that means things could be unclear or inconsistently applied, so we should where at all possible, stay with the process.