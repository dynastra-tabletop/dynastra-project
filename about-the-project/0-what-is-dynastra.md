---
label: What is Dynastra Tabletop?
icon: repo
order: 99
---
Dynastra Tabletop is a Free Software system for allowing users to run tabletop games using a web browser. Traditional tabletop role-playing games rely on pen and paper, and there is absolutely nothing wrong with this form of playing games, but some of the aspects of running tabletop games can be simplified with the use of a computer system. 

Lockdowns don't just pose an unique reason for people to play games using a browser and the internet. There are people throughout the world who cannot play together for whatever reason, and having a system which allows people to play together and have good times, is what Dynastra Tabletop is created for.

### Existing non-free and restrictive systems
There are systems like Roll20 and Foundry VTT, which have a great deal of features, but which are not open. These also pretty much entirely driven by DRM-laden content and cannot be exported to other systems, creating a lock-in to one provider or another. They are also not driven by any sense of standards, or any other concerns that FOSS projects can address, and it just sucks to have to pay extra money to have content held hostage.

### Why was it created? Aren't there other systems around?
There are but, they embrace DRM and have clandestine, and non-open processes with regard to their development. Dynastra believes this is the wrong way to run a project, and the wrong way to create open source software - DRM restricts a user's freedom. The main reason it was created is to make a truly FOSS project which is developed and run democratically by a community, and that allows users to play tabletop games in freedom. 