---
label: Project releases
icon: rocket
---

| Version Number | Name | Date released | Notes |
|---|---|---|---|--|
| v0.1.0 | "Mousey" | In Progress | A tribute to a lovely person. |