---
label: Feature discussion
icon: megaphone
order: 98
---

There is only one developer and a community of one, so I take this opportunity for 100% unity to define the features.

## Features
* User Login and logout
    * User can log into the UI using OIDC provider
    * Users without authentication cannot view UI
    * Support for banned/disabled/locked/etc accounts
    * Token refresh when UI is not idle
    * Display "about to be logged out" as token is about to expire
    * User can log out
    * User can log in
* User Profile
    * User's profile loaded from OIDC provider
    * Link to user profile in OIDC provider
* User Roles
    * Dungeon Master User
    * Player User
    * Visitor User (No roles)
* Dungeon Master User Mode
    * Maps CRUD
    * Tokens CRUD
    * Fog of War (Subtractive only)
    * Move players to map
    * Switch map
* User Mode
    * Current map display
    * "My Tokens" display
* Game Board
    * Add images to map (Dungeon Master only)
    * Drag images on map (Dungeon Master only)
    * Tokens
        * Drag tokens to map
        * Move tokens on map
        * Delete tokens from map
        * Display name below token
    * Objects on map are synced via sockets
* Tokens
    * Add image
    * Add overlay icon (Showing status)
    * Add overlay text (i.e. "Dead", HP, etc)
    * Add notes/description
    * Decorative border (circular, square)
* Communication
    * No-storage chat broadcast (Intentionally included because insecure chat is a bad idea - thanks Ed Snowden <3) - Later will be replaced with Matrix.org
* Dice Rolls
    * Dice rolls in chat (Not stored)
    * Separate display for dice rolls (Dice roll history) - stored
    * Toolbar for dice rolls
* Dice Roll Display
    * Show title
    * Advantage/Disadvantage
    * DC display with success/fail
