---
label: V0.1.0-pre-alpha ("Mousey")
---

# Dynastra Tabletop v0.1.0-pre-alpha ("Mousey")
![**Ironmouse** A lovely and wonderful person with an inspirational background, who is the inspiration for this release and who helped me personally through some tough times. _Image copyright VShoujo, Inc._](./mousey.gif)
=== Release quote
*Meditation brings wisdom; lack of meditation leaves ignorance. Know well what leads you forward and what holds you back, and choose the path that leads to wisdom.*
===

## About this release
This release is the first planned release for Dynastra Tabletop. This document outlines the planned features and basic specification for the Dynastra Tabletop alpha.
The intention of this release is to allow a Player User and a Dungeon Master User to both play, and run a basic game, respectively. 
