---
label: Version Definition
icon: number
order: 99
---

This is the initial version (v0.0.1-pre-alpha), with 1 developer working on it. Here's to Dynastra, you guys.